 /*
	* ====================================================================
	* Copyright (c) 2012-2023 AbandonedCart.  All rights reserved.
	*
	* https://github.com/AbandonedCart/AbandonedCart/blob/main/LICENSE#L4
	* ====================================================================
	*
	* The license and distribution terms for any publicly available version or
	* derivative of this code cannot be changed.  i.e. this code cannot simply be
	* copied and put under another distribution license
	* [including the GNU Public License.] Content not subject to these terms is
	* subject to to the terms and conditions of the Apache License, Version 2.0.
	*/

var path = window.location.pathname;
var page = path.split("/").pop();

if (page.length <= 0)
page = "index.html";

var sidenav = document.getElementById("sidenav");

var manuals = document.createElement( "a" );
manuals.setAttribute("class", "dropdown");
manuals.innerHTML = '<img src="img/icon/tagmo.png">Manuals';
sidenav.appendChild(manuals);

var tutorial = document.createElement( "a" );
tutorial.setAttribute("class", "dropitem");
tutorial.setAttribute("href", "index.html");
tutorial.innerHTML = '<img src="img/icon/tagmo.png">Basic Use';
manuals.appendChild(tutorial);

var properties = document.createElement( "a" );
properties.setAttribute("class", "dropitem");
properties.setAttribute("href", "properties.html");
properties.innerHTML = '<img src="img/icon/tagmo.png">Edit Props';
manuals.appendChild(properties);

var legend = document.createElement( "a" );
legend.setAttribute("class", "dropitem");
legend.setAttribute("href", "legend.html");
legend.innerHTML = '<img src="img/icon/tagmo.png">LoZ Table';
manuals.appendChild(legend);

manuals.addEventListener("click", function() {
	var contents = this.getElementsByTagName("a");
	for (var i = 0; i < contents.length; i++) {
		if (contents[i].style.display === "block") {
			contents[i].style.display = "none";
		} else {
			contents[i].style.display = "block";
		}
	}
});

var foomiibo = document.createElement( "a" );
foomiibo.setAttribute("href", "foomiibo.html");
foomiibo.setAttribute("class", "menuitem");
foomiibo.innerHTML = '<img src="img/icon/foomiibo.png">Foomiibo';
sidenav.appendChild(foomiibo);

var devices = document.createElement( "a" );
devices.setAttribute("class", "dropdown");
devices.innerHTML = '<img src="img/icon/nfc-icon.png">Devices';
sidenav.appendChild(devices);

var bluup = document.createElement( "a" );
bluup.setAttribute("class", "dropitem");
bluup.setAttribute("href", "bluup.html");
bluup.innerHTML = '<img src="https://github.com/BluupLabs/FlaskDesktopApp/raw/main/out/static/img/android-chome-192x192.png">Bluup Labs';
devices.appendChild(bluup);

var keyfobs = document.createElement( "a" );
keyfobs.setAttribute("class", "dropitem");
keyfobs.setAttribute("href", "keyfobs.html");
keyfobs.innerHTML = '<img src="img/icon/amiloop.png">Key Fobs';
devices.appendChild(keyfobs);

var n2elite = document.createElement( "a" );
n2elite.setAttribute("class", "dropitem");
n2elite.setAttribute("href", "n2elite.html");
n2elite.innerHTML = '<img src="img/icon/n2_elite.png">N2 Elite';
devices.appendChild(n2elite);

var powertags = document.createElement( "a" );
powertags.setAttribute("class", "dropitem");
powertags.setAttribute("href", "powertags.html");
powertags.innerHTML = '<img src="img/icon/powertag.png">Power Tags';
devices.appendChild(powertags);

devices.addEventListener("click", function() {
	var contents = this.getElementsByTagName("a");
	for (var i = 0; i < contents.length; i++) {
		if (contents[i].style.display === "block") {
			contents[i].style.display = "none";
		} else {
			contents[i].style.display = "block";
		}
	}
});

var external = document.createElement( "a" );
external.setAttribute("href", "external.html");
external.setAttribute("class", "menuitem");
external.innerHTML = '<img src="img/icon/iphone.png">iOS / 3DS';
sidenav.appendChild(external);

var github = document.createElement( "a" );
github.setAttribute("class", "dropdown");
github.innerHTML = '<img src="img/icon/github.svg">Git Source';
sidenav.appendChild(github);

var tagmosrc = document.createElement( "a" );
tagmosrc.setAttribute("href", "https://github.com/HiddenRamblings/TagMo");
tagmosrc.setAttribute("target", "_blank");
tagmosrc.innerHTML = '<img src="img/icon/github.svg"><div class="tooltip">TagMo<span class="tiptext">HiddenRamblings</span></div>';
github.appendChild(tagmosrc);

var apisrc = document.createElement( "a" );
apisrc.setAttribute("href", "https://github.com/n3evin/amiiboapi");
apisrc.setAttribute("target", "_blank");
apisrc.innerHTML = '<img src="img/icon/github.svg"><div class="tooltip">AmiiboAPI<span class="tiptext">N3evin</span></div>';
github.appendChild(apisrc);

var amiitool = document.createElement( "a" );
amiitool.setAttribute("href", "https://github.com/socram8888/amiitool");
amiitool.setAttribute("target", "_blank");
amiitool.innerHTML = '<img src="img/icon/github.svg"><div class="tooltip">amiitool<span class="tiptext">socram8888</span></div>';
github.appendChild(amiitool);

var wumiibosrc = document.createElement( "a" );
wumiibosrc.setAttribute("href", "https://github.com/hax0kartik/amiibo-generator");
wumiibosrc.setAttribute("target", "_blank");
wumiibosrc.innerHTML = '<img src="img/icon/github.svg"><div class="tooltip">Generator<span class="tiptext">hax0kartik</span></div>';
github.appendChild(wumiibosrc);

var contents = github.getElementsByTagName("a");
for (var i = 0; i < contents.length; i++) {
	contents[i].setAttribute("class", "dropitem");
	contents[i].setAttribute("rel", "external");
}

github.addEventListener("click", function() {
	var contents = this.getElementsByTagName("a");
	for (var i = 0; i < contents.length; i++) {
		if (contents[i].style.display === "block") {
			contents[i].style.display = "none";
		} else {
			contents[i].style.display = "block";
		}
	}
});

var espruino = document.createElement( "a" );
espruino.setAttribute("href", "https://8bitdream.github.io/ntag215-puck.js/");
espruino.setAttribute("target", "_blank");
espruino.setAttribute("class", "menuitem");
espruino.innerHTML = '<img src="img/icon/espruino.png">Espruino';
sidenav.appendChild(espruino);

var support = document.createElement( "a" );
support.setAttribute("href", "donate.html");
support.setAttribute("class", "menuitem");
support.innerHTML = '<img src="img/icon/sponsor.svg">Donations';
sidenav.appendChild(support);

sidenav.appendChild(document.createElement("br"));

var disclaimer = document.createElement( "a" );
disclaimer.setAttribute("href", "privacy.html");
disclaimer.innerHTML = '<img class="disclaimer" src="img/disclaimer.png">';
sidenav.appendChild(disclaimer);

var counter = document.createElement( "a" );
counter.setAttribute("href", "#");
counter.innerHTML = '<img class="counter" src="https://camo.githubusercontent.com/567878163405d733a8a2f2981bdce8c1cd122cffb7fc7dee9109ea24fe128fa7/68747470733a2f2f636f756e742e6765746c6f6c692e636f6d2f6765742f403a7461676d6f3f7468656d653d67656c626f6f7275">';
sidenav.appendChild(counter);

var menubtn = document.getElementById("menubtn");

function toggleNav() {
	localStorage.setItem('menuOpened', true);
	if (menubtn.getAttribute("class") == "opened") {
		sidenav.style.width = "0";
		menubtn.style.paddingLeft = "0";
		sidenav.style.zIndex = "0";
		menubtn.setAttribute("class", "closed");
	} else {
		menubtn.setAttribute("class", "opened");
		sidenav.style.zIndex = "10";
		sidenav.style.width = "280px";
		menubtn.style.paddingLeft = "280px";
	}
}

menubtn.setAttribute("onclick", "toggleNav()");
menubtn.innerHTML = '<img src="img/menubtn.png"><br />Menu / 메뉴';

function displayNav() {
	localStorage.setItem('menuOpened', true);
	if (menubtn.getAttribute("class") != "opened") {
		menubtn.setAttribute("class", "opened");
		sidenav.style.zIndex = "10";
		sidenav.style.width = "280px";
		menubtn.style.paddingLeft = "280px";
	}
}

var translate = document.createElement( "div" );
translate.setAttribute("class", "translate");
var webapp = document.createElement( "a" );
webapp.setAttribute("href", "https://tagmo-gitlab-io.translate.goog/?_x_tr_sl=en&_x_tr_tl=es&_x_tr_pto=wapp");
webapp.innerHTML = '<img src="img/icon/translate.svg">';
translate.appendChild(webapp);
sidenav.parentNode.insertBefore(translate, sidenav.nextSibling);

function getScript(source, callback) {
	var script = document.createElement('script');
	var prior = document.getElementsByTagName('script')[0];
	script.async = 1;
	script.defer = 1;

	script.onload = script.onreadystatechange = function( _, isAbort ) {
		if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
			script.onload = script.onreadystatechange = null;
			script = undefined;

			if (!isAbort) { if (callback) callback(); }
		}
	};

	script.src = source;
	prior.parentNode.insertBefore(script, prior);
}

getScript('https://cdn.jsdelivr.net/npm/@widgetbot/crate@3', function() {
	const crate = new Crate({
		server: '1109571129242296320',
		channel: '1109571228747956334',
		// shard: 'https://globalizer-bot.herokuapp.com',
		defer: true
	});
	crate.options.color = '#7289DA';

	switch(page) {
		case "foomiibo.html":
		crate.options.channel = '1109609945550299236';
		break;
		case "external.html":
		crate.options.channel = '1109846593470992424';
		break;
		case "legend.html":
		crate.options.channel = "1110911745922433034";
		break;
		case "flask.html":
		case "n2elite.html":
		crate.options.channel = '1110198733112615034';
		break;
		case "donate.html":
		crate.hide()
		break;
	}

	crate.on('message', ({ message, channel }) => {
		// Enables notification of new messages
	});
});
