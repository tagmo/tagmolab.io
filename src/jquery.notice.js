 /*
	* ====================================================================
	* Copyright (c) 2021-2023 AbandonedCart.  All rights reserved.
	*
	* https://github.com/AbandonedCart/AbandonedCart/blob/main/LICENSE#L4
	* ====================================================================
	*
	* The license and distribution terms for any publicly available version or
	* derivative of this code cannot be changed.  i.e. this code cannot simply be
	* copied and put under another distribution license
	* [including the GNU Public License.] Content not subject to these terms is
	* subject to to the terms and conditions of the Apache License, Version 2.0.
	*/

var path = window.location.pathname;
var page = path.split("/").pop();

if (page.length <= 0)
page = "index.html";

if (page == "foomiibo.html" || page == "privacy.html") {
	var h1Tag = document.createElement('h1');
	h1Tag.setAttribute("class", "static");
	h1Tag.style.display = "block";
	h1Tag.style.paddingTop = "10px";
	h1Tag.style.paddingBottom = "8px";
	h1Tag.style.fontSize = "0.8em";
	h1Tag.style.fontWeight = "bold";
	h1Tag.textContent = "Foomiibo Disclaimer";
	var divTag = document.createElement('div');
	divTag.setAttribute("class", "static");
	divTag.style.display = "block";
	var pTag = document.createElement('p');
	divTag.appendChild(pTag);
	var centerTag = document.createElement('center');
	pTag.appendChild(centerTag);
	var iTag = document.createElement('i');
	iTag.style.fontSize = "0.9em";
	iTag.textContent = "Foomiibo are generated from public domain information, empty data, and the documented file structure that represent a binary. Serial numbers are assigned to this data using a random number generator and not derived from any specific entity. Foomiibo are not intended to replace any other product, paid or promotional. Files are signed \"TagMo 8-Bit NTAG\""
	centerTag.appendChild(iTag);
	var accordian = document.getElementsByClassName("accordion")[0];
	accordian.appendChild(h1Tag);
	accordian.appendChild(divTag);
}

var h1Tag = document.createElement('h1');
h1Tag.setAttribute("class", "static");
h1Tag.style.display = "block";
h1Tag.style.paddingTop = "10px";
h1Tag.style.paddingBottom = "8px";
h1Tag.style.fontSize = "0.8em";
h1Tag.style.fontWeight = "bold";
h1Tag.textContent = "TagMo Disclaimer";
var divTag = document.createElement('div');
divTag.setAttribute("class", "static");
divTag.style.display = "block";
var pTag = document.createElement('p');
divTag.appendChild(pTag);
var centerTag = document.createElement('center');
pTag.appendChild(centerTag);
var iTag = document.createElement('i');
iTag.style.fontSize = "0.9em";
iTag.textContent = "TagMo is not affiliated, authorized, sponsored, endorsed, or in any way connected with Nintendo Co., Ltd or its subsidiaries. amiibo is a registered trademark of Nintendo of America Inc. TagMo does not claim ownership of any licensed resources. Files created with or resulting from TagMo are not intended for sale or distribution. TagMo is for educational and archival purposes only."
centerTag.appendChild(iTag);
var accordian = document.getElementsByClassName("accordion")[0];
accordian.appendChild(h1Tag);
accordian.appendChild(divTag);
