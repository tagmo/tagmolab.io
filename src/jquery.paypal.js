 /*
  * ====================================================================
  * Copyright (c) 2021-2023 AbandonedCart.  All rights reserved.
  *
  * https://github.com/AbandonedCart/AbandonedCart/blob/main/LICENSE#L4
  * ====================================================================
  *
  * The license and distribution terms for any publicly available version or
  * derivative of this code cannot be changed.  i.e. this code cannot simply be
  * copied and put under another distribution license
  * [including the GNU Public License.] Content not subject to these terms is
  * subject to to the terms and conditions of the Apache License, Version 2.0.
  */

 $.getScript("https://www.paypalobjects.com/donate/sdk/donate-sdk.js", function(data, textStatus, jqxhr) {
	 $('.paypal').removeAttr('href');
	 $('.paypal').html('');
	 PayPal.Donation.Button({
		 env:'production',
		 hosted_button_id:'Q2LFH2SC8RHRN',
		 image: {
			 src:'img/icon/paypal.svg',
			 alt:'Donate with PayPal',
			 title:'PayPal - The safer, easier way to pay online!',
		 }
	 }).render('.paypal');
	 $('.paypal').append('<span id="donate-text">Donations</span>');
	 $('#donate-text').on('click', event => {
		 $('#donate-button').click();
	 });
 });
