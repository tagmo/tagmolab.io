Modules.addCached("TagFactory", function () {
    function d(b, a, c, f, e) {
        this.tagList = b.getAllTags();
        this.curTag = 0;
        this.tagStorage = b;
        this._templ = f || {};
        this._screen = e || void 0;
        this._uploadData = new Uint8Array(540);
        this.uploading = !1;
        this.sort = a;
        this._tag = c.create({ data: new Uint8Array(540) });
        this.reload();
    }
    d.prototype.status = function () {
        return this.tagStorage.getStatus();
    };
    d.prototype.find = function (b) {
        for (var a = 0; a < this.tagList.length; a++) if (this.tagList[a] === b) return a;
        throw "no such element";
    };
    d.prototype.setTag = function (b) {
        this.curTag = this.find(b);
        this.reload();
        return this.get();
    };
    d.prototype.next = function () {
        if (this.tagList.length) return (this.curTag = (this.curTag + 1) % this.tagList.length), this.reload(), this.get();
    };
    d.prototype.refreshTaglist = function () {
        this.tagList = this.tagStorage.getAllTags();
        this.updateScreen();
    };
    d.prototype.updateScreen = function () {
        this._screen.drawTagList(this.total(), this.curTag, this.get().name);
    };
    d.prototype.previous = function () {
        if (this.tagList.length) return (this.curTag = 0 > this.curTag - 1 ? this.tagList.length - 1 : (this.curTag - 1) % this.tagList.length), this.reload(), this.get();
    };
    d.prototype.reload = function (b) {
        if (this.tagList.length) {
            var a = {};
            this._templ.sig && (a.sig = this._templ.sig);
            this._templ.pack && (a.pack = this._templ.pack);
            this._templ.ev1 && (a.ev1 = this._templ.ev1);
            a.data = b ? b : this.tagStorage.loadElement(this.tagList[this.curTag]);
            b || this.updateScreen();
            180 >= a.data.length && ((a.ev1 = [0, 4, 4, 2, 1, 0, 15, 3]), (a.pack = [10]), (a.sig = []));
            this._tag.setTag(a);
            global["\u00ff"].history = [];
        }
    };
    d.prototype.fixUID = function (b) {
        var a = b[0] ^ b[1] ^ b[2] ^ 136,
            c = b[4] ^ b[5] ^ b[6] ^ b[7];
        if (b[3] != a || b[8] != c) (b[3] = a), (b[8] = c);
        b[9] = 72;
        return b;
    };
    d.prototype.createBlank = function (b) {
        for (var a = new Uint8Array(7), c = 0; c < a.length; c++) a[c] = Math.round(255 * Math.random());
        c = {};
        this._uploadData.fill(0, 0, 540);
        a[0] = 4;
        c.uid = a;
        c.data = this._uploadData;
        a = this._tag.setTag(c);
        c.ev1 = [];
        c.pack = [];
        c.sig = [];
        c.data.set(a, 0);
        c.data.set([0, 0, 225, 16, 62, 0, 0, 3, 0, 254], 10);
        this._tag.setData(c);
        b = b ? b : "New Tag |" + ((10 * getTime()) | 0) + "|0";
        this.tagStorage.storeElement(b, c.data);
        this.tagList = this.tagStorage.getAllTags();
        this.curTag = this.tagList.length - 1;
        1 === this.tagList.length && (hasTags = !0);
        this.reload();
    };
    d.prototype.store = function (b) {
        if (this.tagList.length) {
            if (b) {
                var a = new Uint8Array(b),
                    c = a;
                1 == this._screen.menuActive && ((this._screen.menuActive = !1), (this._screen.currentMenu = 0));
                this.reload(a);
            } else c = this._tag.getTag().data;
            a = this.tagList[this.curTag];
            if (0 >= a.indexOf("|") && !N2MODE) {
                var f = "";
                new Uint8Array(b.buffer, 88, 4).forEach(function (e) {
                    e = String(e.toString(16));
                    f += 1 === e.length ? "0" + e : e;
                });
                b = parseInt(f, 16).toString(36);
                a = a.substring(0, 20) + "|" + b;
            }
            this.tagStorage.storeElement(a, c);
            a !== this.tagList[this.curTag] &&
                (this.tagStorage.removeElement(this.tagList[this.curTag]), (this.tagList = this.tagStorage.getAllTags()), (this.curTag = this.tagList.length - 1), this.reload(c), Bluetooth.println(JSON.stringify({ event: "refresh" })));
        }
    };
    d.prototype.get = function () {
        return { index: this.curTag, name: this.tagList[this.curTag] };
    };
    d.prototype.getData = function () {
        return { name: this.tagList[this.curTag], data: this._tag };
    };
    d.prototype.getList = function (b, a) {
        return "undefined" === typeof b ? this.tagList : this.tagList.slice(b, b + a);
    };
    d.prototype.getStatus = function () {
        return !0;
    };
    d.prototype.total = function () {
        return this.tagList.length;
    };
    d.prototype.rename = function (b, a) {
        this.tagStorage.removeElement(b);
        this.tagStorage.storeElement(a, this._tag.getTag().data);
        this.tagList = this.tagStorage.getAllTags();
        this.curTag = this.tagList.indexOf(a);
        this.updateScreen();
        return this.get();
    };
    d.prototype.remove = function (b) {
        this.curTag === this.tagList.length - 1 && (this.curTag = this.tagList.length - 2);
        this.tagStorage.removeElement(b);
        this.tagList = this.tagStorage.getAllTags();
        this.reload();
        0 === this.tagList.length && this._screen.splash();
        return this.tagStorage.getStatus();
    };
    d.prototype.startTagUpload = function (b) {
        this._uploadOffset = 0;
        this._uploadSize = b;
        this._screen.drawUploadProgress(this.uploading);
        this.uploading = !0;
        setInput(!1);
        return this._uploadOffset;
    };
    d.prototype.tagUploadChunk = function (b) {
        b = E.toArrayBuffer(atob(b));
        this._uploadData.set(b, this._uploadOffset);
        return (this._uploadOffset += b.length);
    };
    d.prototype.saveUploadedTag = function (b) {
        this.tagStorage.storeElement(b, this._uploadData);
        global["\u00ff"].history = [];
        return this.tagStorage.getStatus();
    };
    d.prototype.upload = function (b, a, c, f) {
        1 == a && (this._screen.drawUploadProgress(this.uploading), (this.uploading = !0), setInput(!1));
        f = E.toArrayBuffer(atob(f));
        this.tagStorage.storeElement(b, f);
        a == c && ((global["\u00ff"].history = []), (this.tagList = this.tagStorage.getAllTags()), (this.uploading = !1), 1 === this.tagList.length && this.setTag(this.tagList[0]), (hasTags = !0), setInput(!0), this.updateScreen());
        return this.tagStorage.getStatus();
    };
    d.prototype.uploadsComplete = function () {
        global["\u00ff"].history = [];
        this.tagList = this.tagStorage.getAllTags();
        this.uploading = !1;
        1 === this.tagList.length && this.setTag(this.tagList[0]);
        hasTags = !0;
        setInput(!0);
        this.updateScreen();
        return this.tagStorage.getStatus();
    };
    d.prototype.download = function (b) {
        return this.tagStorage.loadElement(b);
    };
    exports.create = function (b, a, c, f, e) {
        return new d(b, a, c, f, e);
    };
});
Modules.addCached("encrypt", function () {
    global.KEYS = void 0;
    global.rotateCount = 0;
    global.menuCount = 2;
    if (!global.KEYS) {
        var d = require("Storage").readArrayBuffer(".ek");
        "undefined" === typeof d ? ((global.KEYS = !1), (global.menuCount = 2)) : ((global.KEYS = new Uint8Array(d)), (global.menuCount = 3));
    }
    exports = {
        randomizeUid: function () {
            if (global.KEYS) {
                var b = new Uint8Array(tag._tag._data);
                amiitool.unpack(KEYS, b);
                var a = new Uint8Array(9);
                a[0] = 4;
                a[1] = Math.round(255 * Math.random());
                a[2] = Math.round(255 * Math.random());
                a[3] = a[0] ^ a[1] ^ a[2] ^ 136;
                a[4] = Math.round(255 * Math.random());
                a[5] = Math.round(255 * Math.random());
                a[6] = Math.round(255 * Math.random());
                a[7] = Math.round(255 * Math.random());
                a[8] = a[4] ^ a[5] ^ a[6] ^ a[7];
                var c = new Uint8Array(4);
                c[0] = 170 ^ a[1] ^ a[3];
                c[1] = 85 ^ a[2] ^ a[4];
                c[2] = 170 ^ a[3] ^ a[5];
                c[3] = 85 ^ a[4] ^ a[6];
                b.set(a.slice(0, 8), 468);
                b.set(c, 532);
                b[0] = a[8];
                amiitool.pack(KEYS, b);
                tag.reload(b);
            }
        },
    };
});
Modules.addCached("Screen", function () {
    function d(c) {
        return 99 < c ? { x: 36, circleX: 0 } : 10 > c ? { x: 24, circleX: -12 } : { x: 36, circleX: -6 };
    }
    function b(c) {
        "0" == c && (a.prototype.drawTagList = a.prototype.drawTagListTopAndBottom);
        "1" == c && (a.prototype.drawTagList = a.prototype.drawTagListRounded);
    }
    function a(c, f) {
        this.currentMenu = this.inter = 0;
        this.menuActive = !1;
        b(c);
        I2C1.setup({ scl: 6, sda: 7 });
        this.screen = require("SSD1306New").connect(
            I2C1,
            function () {
                f();
            },
            { height: 32, rst: 30 }
        );
        return this;
    }
    a.prototype.on = function () {
        this.screen.on();
        this.screen.clear();
    };
    a.prototype.splash = function () {
        this.screen.clear();
        var c = this.screen,
            f = {
                width: 32,
                height: 32,
                bpp: 1,
                buffer: E.toArrayBuffer(atob("AAAAAAAAcAAAAFAAAAZQAAAOcAAABgAAAAAAAAAP8AAACBAAAA4wAAACQAAAAkAAAAJAAAAOcAAAGBgAACAEAABAAgAAgAEAAIABAAEAAIABAACAAYfhgAF4HoABAACAASAAgACgAQAAkAEAAEwCAAAnBAAAGBgAAAfgAAAAAAA=")),
            };
        c.drawImage(f, 10, 0);
        this.screen.setFontVector(28);
        this.screen.drawString("Flask", 44, 6);
        this.screen.flip();
    };
    a.prototype.reset = function () {
        this.screen.reset();
    };
    a.prototype.drawUploadProgress = function (c) {
        var f = { width: 28, height: 16, bpp: 1, buffer: E.toArrayBuffer(atob("KqAABVUAAP/4D4//gPgAAA+P/4D4wBgPiAyA+IAof/gAg/6ACB/IAID4wBgHD/+AIJ/4AA//gAA=")) };
        c || (this.screen.clear(), this.screen.drawImage(f, 52, 8), this.screen.flip());
        return null;
    };
    a.prototype.showMenu = function () {
        2 === this.currentMenu &&
            (this.screen.clear(),
            this.screen.drawLine(4, 16, 8, 22),
            this.screen.drawLine(4, 16, 8, 10),
            this.screen.drawLine(124, 16, 120, 22),
            this.screen.drawLine(124, 16, 120, 10),
            this.screen.setFontVector(16),
            this.screen.drawString("(" + global.rotateCount.toString() + ")Cycle", 22, 10),
            this.screen.flip());
        1 === this.currentMenu &&
            (this.screen.clear(),
            this.screen.drawLine(4, 16, 8, 22),
            this.screen.drawLine(4, 16, 8, 10),
            this.screen.drawLine(124, 16, 120, 22),
            this.screen.drawLine(124, 16, 120, 10),
            this.screen.setFontVector(16),
            this.screen.drawString("Delete Tag", 22, 10),
            this.screen.flip());
        0 === this.currentMenu &&
            (this.screen.clear(),
            this.screen.drawLine(4, 16, 8, 22),
            this.screen.drawLine(4, 16, 8, 10),
            this.screen.drawLine(124, 16, 120, 22),
            this.screen.drawLine(124, 16, 120, 10),
            this.screen.setFontVector(16),
            this.screen.drawString("Exit Menu", 26, 10),
            this.screen.flip());
    };
    a.prototype.drawTagListTopAndBottom = function (c, f, e) {
        this.screen.clear();
        e = e.split("|")[0];
        this.screen.setFontVector(10);
        this.screen.drawString(String(f + 1) + "/" + String(c), 2, 2);
        this.screen.setFontVector(16);
        12 < e.length && this.screen.setFontVector(14);
        this.screen.drawString(e, 2, 14);
        batteryLow && this.lowBattery();
        this.screen.flip();
    };
    a.prototype.drawTagListRounded = function (c, f, e) {
        var h = d(c).x;
        d(c);
        var k = d(c).circleX;
        this.screen.clear();
        e = e.split("|")[0];
        this.screen.setFontVector(11);
        this.screen.drawString(String(f + 1), 2, 2);
        this.screen.drawCircle(k, 15, 30);
        this.screen.drawString(String(c), 2, 19);
        this.screen.setFontVector(16);
        batteryLow ? (this.screen.drawString(e, h, 14), this.lowBattery()) : this.screen.drawString(e, h, 8);
        this.screen.flip();
    };
    a.prototype.lowBattery = function () {
        this.screen.drawRect(104, 5, 106, 8);
        this.screen.drawRect(106, 3, 126, 10);
        this.screen.fillRect(108, 5, 111, 8);
        this.screen.flip();
    };
    a.prototype.needsCharge = function () {
        this.screen.clear();
        this.screen.setFontVector(16);
        this.screen.drawString("Please Turn off", 2, 2);
        this.screen.drawString("and Charge", 20, 16);
        this.screen.flip();
    };
    a.prototype.setFace = function (c) {
        require("Storage").write(".config", c);
        b(c);
        tag.updateScreen();
    };
    a.prototype.getVersion = function () {
        return { v: "7.0" };
    };
    exports.init = function (c, f) {
        return new a(c, f);
    };
});
encrypt = require("encrypt");
inputEnabled = !0;
batteryAlert = batteryLow = hasTags = !1;
sortConfig = 0;
function saveTag(d) {
    tag.store(d);
}
function setInput(d) {
    inputEnabled = !0 === d ? !0 : !1;
}
E.on("init", function () {
    E.srand(E.hwRand());
    NRF.setAdvertising({}, { interval: 1e3 });
    var d = require("TagStorage").init(),
        b = require("NTAGFactory"),
        a = { pack: [128, 128], ev1: [0, 4, 4, 2, 1, 0, 17, 3] },
        c,
        f = require("Storage").read(".config") || 0;
    sortConfig = require("Storage").read(".sort") || 0;
    d.__proto__.getAllTags = function () {
        var e = d._storage.list(/^[a-zA-Z0-9]/);
        d.tagCount = e.length;
        return 0 != sortConfig ? e.sort() : e;
    };
    screen = require("Screen").init(f, function () {
        function e() {
            h && (3.2 > NRF.getBattery() && ((batteryLow = !0), screen.lowBattery()), 3.1 > NRF.getBattery() && ((batteryLow = !0), screen.needsCharge(), (batteryAlert = !0), NRF.sleep(), (h = inputEnabled = !1)), setTimeout(e, 6e5));
        }
        tag = require("TagFactory").create(d, sortConfig, b, a, screen);
        tag._tag.__proto__._read = function (g) {
            g = 4 * g[1];
            g = this.written ? new Uint8Array(this.writtenData.buffer, g, 16) : new Uint8Array(this._data.buffer, g, 16);
            NRF.nfcSend(g);
        };
        var h = !0;
        e();
        var k = tag.get();
        0 < tag.total() && (hasTags = !0);
        if (batteryAlert) screen.on();
        else
            hasTags
                ? setTimeout(function () {
                      screen.drawTagList(tag.total(), tag.curTag, k.name);
                      screen.on();
                  }, 10)
                : setTimeout(function () {
                      screen.splash();
                      screen.on();
                  }, 10);
    });
    leftWatch = setWatch(
        function () {
            if (inputEnabled)
                if (screen.menuActive) {
                    var e = 5 <= tag.get().name.split("|")[1].length ? global.menuCount : 2;
                    screen.currentMenu = 0 > screen.currentMenu - 1 ? e - 1 : (screen.currentMenu - 1) % e;
                    screen.showMenu();
                } else hasTags && ((c = tag.previous()), (c.event = "button"), Bluetooth.println(JSON.stringify(c)));
        },
        BTN,
        { edge: "rising", debounce: 50, repeat: !0 }
    );
    middleWatch = setWatch(
        function () {
            inputEnabled &&
                hasTags &&
                (!1 === screen.menuActive
                    ? ((screen.menuActive = !0), screen.showMenu())
                    : (1 === screen.currentMenu && (tag.remove(tag.get().name), Bluetooth.println(JSON.stringify({ event: "delete" })), (screen.menuActive = !1), tag.updateScreen(), (screen.currentMenu = 0)),
                      0 === screen.currentMenu && ((screen.menuActive = !1), tag.updateScreen(), (screen.currentMenu = 0), (global.rotateCount = 0)),
                      2 === screen.currentMenu && (encrypt.randomizeUid(), (global.rotateCount += 1), screen.showMenu())));
        },
        BTN2,
        { edge: "rising", debounce: 50, repeat: !0 }
    );
    rightWatch = setWatch(
        function () {
            if (inputEnabled)
                if (screen.menuActive) {
                    var e = 5 <= tag.get().name.split("|")[1].length ? global.menuCount : 2;
                    screen.currentMenu = (screen.currentMenu + 1) % e;
                    screen.showMenu();
                } else hasTags && ((c = tag.next()), (c.event = "button"), Bluetooth.println(JSON.stringify(c)));
        },
        BTN3,
        { edge: "rising", debounce: 50, repeat: !0 }
    );
});
